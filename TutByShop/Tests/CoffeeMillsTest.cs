﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using TutByShop.Core.Navigation;
using TutByShop.UI.Base;

namespace TutByShop.Tests
{
    public class CoffeeMillsTest: BaseTest
    {
        [TestCase("Bosch", 1)]
        [TestCase("VITEK", 1)]
        public void ValidatePagesCount(string companyName, int expectedPAgesCount)
        {
            BaseItemsSectionPage itemsPage = Navigator.OpenItemSectionPage("Coffee Mills");
            itemsPage.SelectCompany(companyName);
            Assert.AreEqual(itemsPage.NumberOfPagesInSearchResults, expectedPAgesCount);
        }
    }
}
