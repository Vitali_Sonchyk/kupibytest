﻿
using NUnit.Framework;

using TutByShop.UI.Base;
using TutByShop.Core.Navigation;

namespace TutByShop.Tests
{
    public class WaterFiltersTest: BaseTest
    {
        [Test]
        public void ValidateWaterFilters()
        {
            BaseItemsSectionPage itemsPage = Navigator.OpenItemSectionPage("Water Filters Accessories");
            itemsPage.SelectCompany("Аквафор");
            Assert.AreEqual(itemsPage.NumberOfPagesInSearchResults, 9);
        }
    }
}
