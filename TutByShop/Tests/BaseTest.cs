﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using OpenQA.Selenium.Firefox;

using TutByShop.Core.DriverFactory;

namespace TutByShop.Tests
{
    public class BaseTest
    {
        [SetUp]
        public void Setup()
        {
            WebDriverFactory.Driver.Navigate().GoToUrl(ConfigurationSettings.AppSettings["homePage"]);
        }

        [TearDown]
        public void TearDown()
        {
            WebDriverFactory.Driver.Quit();
        }
    }
}
