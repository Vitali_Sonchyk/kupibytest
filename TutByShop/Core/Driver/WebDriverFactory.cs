﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

using TutByShop.Core.Attributes.Exceptions;

namespace TutByShop.Core.DriverFactory
{
    public class WebDriverFactory
    {
        private static IWebDriver _driver { get; set; }

        private static IWebDriver GetWebDriver(string browserName)
        {
             IWebDriver driver;
             switch (browserName)
             {
                case "Firefox":
                {
                    driver = new FirefoxDriver();
                    break;
                }

                case "Chrome":
                {
                    driver = new FirefoxDriver();
                    break;
                }
                
                case "Internet Explorer":
                {
                    driver = new InternetExplorerDriver();
                    break;
                }

                 default:
                 {
                     throw new ItemNotFoundException(String.Format("Unexpected browser name was specified: {0}", browserName));
                 }
             }

             return driver;
        }

        public static IWebDriver Driver
        {
            get
            {
                if (_driver == null)
                {
                    return CreateNewWebDriver();
                }
               
                return _driver;
            }
        }

        public static IWebDriver CreateNewWebDriver()
        {
            IWebDriver driver = null;
            if (_driver != null)
            {
                _driver.Quit();
                _driver.Close();
            }

            try
            {
                for (int i = 0; i < 3; i++)
                {
                    string broserName = ConfigurationSettings.AppSettings["browserName"];
                    driver = GetWebDriver(broserName);
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

                    _driver = driver;
                    return _driver;
                }
            }
            catch (WebDriverException ex)
            {
                // TODO: add logic for killing driver processes
            }

            if (driver == null)
            {
                throw new WebDriverException("Failed to create driver after 3 attempts.");
            }

            return _driver;
        }
    }
}
