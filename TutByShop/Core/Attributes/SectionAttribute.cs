﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutByShop.Core.Attributes
{
    public class SectionAttribute : Attribute
    {
        public string SectionName { get; private set; }

        public SectionAttribute(string sectionName)
        {
            this.SectionName = sectionName;
        }
    }
}
