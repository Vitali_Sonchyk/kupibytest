﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutByShop.Core.Helpers
{
    public class ExceptionHelper
    {
        public static T NotNull<T>(T value, Exception ex)
        {
            if (value == null)
            {
                throw ex;
            }

            return value;
        }
    }
}
