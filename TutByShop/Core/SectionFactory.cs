﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using TutByShop.Core.Attributes;
using TutByShop.Core.DriverFactory;
using TutByShop.UI.Base;

namespace TutByShop.Core
{
    public class SectionFactory
    {
        public static BaseItemsSectionPage GetSectionByName(string sectionName)
        {
            Type sectionType =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(t => t.IsClass)
                    .Where(typeof(BaseItemsSectionPage).IsAssignableFrom)
                    .FirstOrDefault(
                        t =>
                        t.GetCustomAttributes<SectionAttribute>().Where(a => a.SectionName.Equals(sectionName)).Count()> 0);

            return (BaseItemsSectionPage)Activator.CreateInstance(sectionType, WebDriverFactory.Driver);
        }
    }
}
