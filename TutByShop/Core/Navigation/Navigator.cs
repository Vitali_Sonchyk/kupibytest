﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TutByShop.Core;
using TutByShop.Core.DriverFactory;
using TutByShop.UI.Base;

namespace TutByShop.Core.Navigation
{
    public class Navigator
    {
        private static BaseItemsSectionPage Navigate(string sectionName, Action<BaseItemsSectionPage> action)
        {
            var targetSectionObject = SectionFactory.GetSectionByName(sectionName);
            action.Invoke(targetSectionObject);

            return targetSectionObject;
        }

        public static BaseItemsSectionPage OpenItemSectionPage(string sectionName)
        {
            return Navigate(sectionName,
                (p) =>
                {
                    List<string> path = p.Path;
                    new BaseCategorySectionPage(WebDriverFactory.Driver).OpenItemsSection(path);
                });
        }
    }
}
