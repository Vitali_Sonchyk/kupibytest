﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;

using TutByShop.Core.Attributes.Exceptions;
using TutByShop.Core.DriverFactory;
using TutByShop.Core.Helpers;

namespace TutByShop.Core.Extensions
{
    public static class WebElementExtensions
    {
        public static IWebElement FindCheckBoxByLabel(this IWebElement parent, string label)
        {
            string labelXpathExpression = String.Format(".//label[normalize-space(text()) = '{0}']", label);

            var labelElement = parent.FindElements(By.XPath(labelXpathExpression)).FirstOrDefault();
            ExceptionHelper.NotNull(
                labelElement,
                new ItemNotFoundException(String.Format("The '{0}' label was not found", label)));

            var checkBoxElement =
                labelElement.FindElements(By.XPath("./preceding-sibling::input[@type='checkbox']")).FirstOrDefault();

            return ExceptionHelper.NotNull(
                checkBoxElement,
                new ItemNotFoundException(String.Format("Checkbox for '{0}' label was not found", label)));
        }

        public static void ScrollIntoView(this IWebElement element)
        {
            ((IJavaScriptExecutor)WebDriverFactory.Driver).ExecuteScript(
                    "arguments[0].scrollIntoView()",
                    element);
        }
    }
}
