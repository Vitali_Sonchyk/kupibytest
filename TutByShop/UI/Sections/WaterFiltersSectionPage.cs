﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.Core.Attributes;
using TutByShop.Core.DriverFactory;
using TutByShop.UI.Base;

namespace TutByShop.UI.Sections
{
    [Section("Water Filters Accessories")]
    public class WaterFiltersSectionPage : BaseItemsSectionPage
    {
        private readonly List<string> _path = new List<string>() { "Для дома", "Аксессуары",  "Аксессуары для водяных фильтров"}; 

        public WaterFiltersSectionPage(IWebDriver driver)
            : base(driver)
        { }

        public override List<string> Path
        {
            get
            {
                return this._path;
            }
        }
    }
}
