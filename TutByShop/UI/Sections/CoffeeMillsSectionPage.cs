﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;

using TutByShop.Core.Attributes;
using TutByShop.UI.Base;

namespace TutByShop.UI.Sections
{
    [Section("Coffee Mills")]
    public class CoffeeMillsSectionPage: BaseItemsSectionPage
    {
        private readonly List<string> _path = new List<string>() { "Для кухни", "Приготовление напитков", "Кофемолки" };

        public CoffeeMillsSectionPage(IWebDriver container)
            : base(container)
        {
        }

        public override List<string> Path
        {
            get
            {
                return this._path;
            }
        }
    }
}
