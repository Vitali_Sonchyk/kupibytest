﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

using TutByShop.Core.DriverFactory;
using TutByShop.UI.Controls;

namespace TutByShop.UI.Base
{
    public abstract class BaseShopPage : IBaseShopPage
    {
        protected IWebDriver _webDriver;

        [FindsBy(How = How.CssSelector, Using = "div.m-bottom_fix")]
        private IWebElement _footerElement { get; set; }

        public BaseShopPage(IWebDriver webdriver)
        {
            this._webDriver = webdriver;
            PageFactory.InitElements(this._webDriver, this);
        }

        public abstract List<string> Path { get; }

        [FindsBy(How = How.CssSelector, Using = "div.b-header")]
        public IWebElement Header { get; set; }

        [FindsBy(How = How.CssSelector, Using = "div.m-bottom_fix")]
        public IWebElement InnerGrid { get; set; }

        public void HideCurrencConverter()
        {
            this.Footer.CurrencyConverter.Hide();
        }

        public Footer Footer {
            get
            {
                return new Footer(_footerElement);
            }
        }
    }

    public class Footer: BaseControl
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'convert_wrapper')]")]
        private IWebElement _currencyConverter;

        public Footer(IWebElement container)
            : base(container)
        {
        }

        public CurrencyConverter CurrencyConverter {
            get
            {
                return new CurrencyConverter(this._currencyConverter);
            }
        }
    }

    public class CurrencyConverter: BaseControl
    {
        [FindsBy(How = How.CssSelector, Using = "a.convert_link")]
        private IWebElement _showHideLink;

        public CurrencyConverter(IWebElement container)
            : base(container)
        {
        }

        public void Hide()
        {
            if (this.Container.GetAttribute("class").Contains("active"))
            {
                this._showHideLink.Click();
            }
        }
    }
}
