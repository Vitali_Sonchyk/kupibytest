﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.Core.DriverFactory;
using TutByShop.UI.Controls;

namespace TutByShop.UI.Base
{
    public class BaseCategorySectionPage: BaseShopPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@class = 'inner grid']")]
        private IWebElement _gridElement { get; set; }

        public BaseCategorySectionPage(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public CategoriesGrid CategoriesGrid {
            get
            {
                return new CategoriesGrid(_gridElement);
            }
        }

        public void OpenItemsSection(List<string> path)
        {
            path.ForEach(pathPart => this.CategoriesGrid.GetCategory(pathPart).Open());
        }

        public override List<string> Path
        {
            get
            {
                return new List<string>();
            }
        }
    }
}
