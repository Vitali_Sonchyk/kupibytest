﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutByShop.UI.Base
{
    public interface IBaseShopPage
    {
        List<string> Path { get; }
    }
}
