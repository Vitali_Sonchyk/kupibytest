﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.UI.Controls;

namespace TutByShop.UI.Base
{
    public abstract class BaseItemsSectionPage: BaseShopPage
    {
         [FindsBy(How = How.XPath, Using = "//div[@class = 'inner grid']")]
        private IWebElement _companiesGrid { get; set; }

        public BaseItemsSectionPage(IWebDriver webDriver):base(webDriver)
        {
        }

        public int NumberOfPagesInSearchResults {
            get
            {
                return this.CompaniesGrid.Pagination.PagesCount;
            }
        }

        public void SelectCompany(string companyName)
        {
            this.HideCurrencConverter();
            this.CompaniesGrid.FiltersForm.SelectBrand(companyName);
            this.CompaniesGrid.FiltersForm.ClickSearchButton();
        }

        public CompaniesGrid CompaniesGrid
        {
            get
            {
                return new CompaniesGrid(_companiesGrid);
            }
        }



    }
}
