﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.Core.Attributes.Exceptions;
using TutByShop.Core.DriverFactory;
using TutByShop.UI.Base;

namespace TutByShop.UI.Controls
{
    public class CategoriesGrid: BaseGrid
    {
        [FindsBy(How = How.XPath, Using = ".//div[@class='cBlock']")]
        private IList<IWebElement> _categoriesElements { get; set; }

        public CategoriesGrid(IWebElement container)
            : base(container)
        {
        }

        public List<Category> Categories
        {
            get
            {
                List<Category> categoriesList = new List<Category>();
                this._categoriesElements.ToList().ForEach(
                    cat =>
                    {
                        categoriesList.Add(new Category(cat));
                    });

                return categoriesList;
            }
        }

        public Category GetCategory(string name)
        {
            return Categories.FirstOrDefault(cat => cat.CategoryName.Equals(name));
        }
    }

    public class Category: BaseControl
    {
        [FindsBy(How = How.XPath, Using = ".//div[@class='cHead']//a")]
        private IWebElement _categoriesLinkElement { get; set; }

        public Category(IWebElement container):base(container)
        {
        }

        public string CategoryName {
            get
            {
                return this._categoriesLinkElement.Text;
            }
        }

        public void Open()
        {
            _categoriesLinkElement.Click();
        }
    }
}
