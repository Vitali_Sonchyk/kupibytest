﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace TutByShop.UI.Controls
{
    public class BaseControl
    {
        protected IWebElement Container;
        
        public BaseControl(IWebElement container)
        {
            this.Container = container;
            PageFactory.InitElements(Container, this);
        }
    }
}
