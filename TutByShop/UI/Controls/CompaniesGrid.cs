﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using NUnit.Framework.Internal;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.Core.Attributes.Exceptions;
using TutByShop.Core.DriverFactory;
using TutByShop.Core.Extensions;

namespace TutByShop.UI.Controls
{
    public class CompaniesGrid: BaseGrid
    {
        [FindsBy(How = How.CssSelector, Using = "div.itemList")]
        private IWebElement _itemsListElement;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'pagination')]")]
        private IWebElement _paginationElement;

        [FindsBy(How = How.CssSelector, Using = "form[name='filters_form']")]
        private IWebElement _filtersForm;

        public CompaniesGrid(IWebElement container)
            : base(container)
        {
        }

        public ItemsList ItemsList 
        {
            get
            {
                return new ItemsList(this._itemsListElement);
            }
        }

        public FiltersForm FiltersForm
        {
            get
            {
                return new FiltersForm(_filtersForm);
            }
        }

        public Pagination Pagination
        {
            get
            {
                return new Pagination(this._paginationElement);
            }
        }
    }

    public class ItemsList :BaseControl
    {
        [FindsBy(How = How.CssSelector, Using = "a.Shoplink")]
        private IList<IWebElement> ItemElements { get; set; }

        public ItemsList(IWebElement container)
            : base(container)
        {
        }

        public IEnumerable<string> Items
        {
            get
            {
                return this.ItemElements.ToList().Select(item => item.Text);
            }
        }

        public void SelectItem(string itemName)
        {
            IWebElement link = this.ItemElements.FirstOrDefault(item => item.Text.Equals(itemName));

            Core.Helpers.ExceptionHelper.NotNull(
                link,
                new ItemNotFoundException(String.Format("The '{0}' item was not found in companies grid", itemName)));

            link.Click();
            
            //TODO:return navigated page should be added
            //TODO:wait for page load should be added
        }
    }

    public class FiltersForm : BaseControl
    {
        [FindsBy(How = How.XPath, Using = "//div[@class = 'cnt']//ul[@id = 'brands'][contains(@class, 'param_list')]")]
        private IList<IWebElement> _brands;

        [FindsBy(How = How.XPath, Using = "//a[@id = 'toggleBrand'][contains(@class, 'toggle')]")]
        private IWebElement _expandLink;

        [FindsBy(How = How.XPath, Using = "//div[@class = 'send']//input[contains(@class, 'button big')]")]
        private IWebElement _searchButton;

        public FiltersForm(IWebElement container)
            : base(container)
        {
        }

        public void SelectBrand(string brandName)
        {
            if (_expandLink.Text.Equals("Все производители"));
            {
                WebElementExtensions.ScrollIntoView(_expandLink);
                _expandLink.Click();
            }

            var checkBoxElement = this.Container.FindCheckBoxByLabel(brandName);
            if (checkBoxElement.Selected)
            {
                Core.Logger.Info(String.Format("The '{0}' checkbox is already checked", brandName));
            }

            else
            {
                checkBoxElement.Click();
            }
        }

        public void ClickSearchButton()
        {
            WebElementExtensions.ScrollIntoView(this._searchButton);
            this._searchButton.Click();
            //TODO: add waitfor ready
        }
    }

    public class Pagination : BaseControl
    {
        [FindsBy(How = How.CssSelector, Using = "li.p-item")]
        private IList<IWebElement> _pageResults;

        [FindsBy(How = How.XPath, Using = "//li[contains(@class, 'prev')]//a[contains(@class, 'inside')]")]
        private IWebElement _previousPage;

        [FindsBy(How = How.XPath, Using = "//li[contains(@class, 'next')]//a[contains(@class, 'inside')]")]
        private IWebElement _nextPage;

        public Pagination(IWebElement container)
            : base(container)
        {
        }

        public int PagesCount
        {
            get
            {
                int count;
                if (this._pageResults.Count() != 0)
                {
                    var lastPageLink = this._pageResults.ToList().Last().FindElements(By.TagName("a")).FirstOrDefault();
                    count = (lastPageLink == null) ? 1 : Int32.Parse(lastPageLink.GetAttribute("title"));

                    return count;
                }

                else
                {
                    count = 1;
                }

                return count;
            }
        }
    }
}
