﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

using TutByShop.Core.DriverFactory;

namespace TutByShop.UI.Controls
{
    public class BaseGrid : BaseControl
    {
        public BaseGrid(IWebElement container)
            : base(container)
        {
        }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'top_row top_row_mod')]")]
        public IWebElement TopRow { get; set; }
    }
}
